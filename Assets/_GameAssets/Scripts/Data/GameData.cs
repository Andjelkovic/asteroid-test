﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets._GameAssets.Scripts.Data
{
    /// <summary>
    /// Game data is used for storing general game data throughout the app
    /// </summary>
    public class GameData
    {
        public GameData()
        {
            Leaderboard = new List<Score>();
        }
        public bool IsPaused;
        public string PlayerName;
        public int CurrentScore;
        public int HighScore;
        public List<Score> Leaderboard;
    }
}
