﻿using UnityEditor;
using UnityEngine;

namespace Assets._GameAssets.Scripts.Data
{
    /// <summary>
    /// Class for individual score points per player
    /// </summary>
    public class Score
    {
        public Score() { }

        public Score(string playerName)
        {
            Id = GUID.Generate();
            PlayerName = playerName;
        }

        public GUID Id { get; set; }
        public string PlayerName { get; set; }
        public int Points { get; set; }
    }
}
