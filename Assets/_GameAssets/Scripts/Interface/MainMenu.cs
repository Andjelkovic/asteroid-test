﻿using Assets._GameAssets.Scripts.Core;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Assets._GameAssets.Scripts.Interface
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField]
        private InputField _playerNameInput;

        [SerializeField]
        private Button _startButon;

        [SerializeField]
        private Button _leaderboardButon;

        [SerializeField]
        private Button _quitButon;

        [SerializeField]
        private GameObject _leaderBoardPanel;


        private GameData _gameData;

        [Inject]
        public void Construct(GameData gameData)
        {
            _gameData = gameData;
        }

        private void OnEnable()
        {
            _startButon.onClick.AddListener(OnStartClicked);
            _leaderboardButon.onClick.AddListener(OnLeaderboardClicked);
            _quitButon.onClick.AddListener(OnQuitClicked);
        }

        private void OnDisable()
        {
            _startButon.onClick.RemoveAllListeners();
            _leaderboardButon.onClick.RemoveAllListeners();
            _quitButon.onClick.RemoveAllListeners();
        }

        private void OnStartClicked()
        {
            gameObject.SetActive(false);
            _gameData.PlayerName = _playerNameInput.text;
            _gameData.IsPaused = false;
            SceneManager.LoadScene(Strings.GameplayScene);
        }

        private void OnLeaderboardClicked()
        {
            _leaderBoardPanel.SetActive(true);
        }

        private void OnQuitClicked()
        {
            // this wont work in editor
            Application.Quit();
        }
    }
}
