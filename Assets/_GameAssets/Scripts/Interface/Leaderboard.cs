﻿using System.Linq;
using Assets._GameAssets.Scripts.Core;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets._GameAssets.Scripts.Interface
{
    public class Leaderboard : MonoBehaviour
    {
        [SerializeField]
        private Text _leaderBoardNames;

        [SerializeField]
        private Text _leaderBoardScores;

        private GameData _gameData;
        
        [Inject]
        public void Construct(GameData gameData)
        {
            _gameData = gameData;
        }

        private void OnEnable()
        {
            var leaderboard = _gameData.Leaderboard.OrderByDescending(item => item.Points).Take(3);

            var names = string.Empty;
            var scores = string.Empty;

            foreach (var score in leaderboard)
            {
                names += string.Format("{0}\n", score.PlayerName);
                scores += string.Format("{0}\n", score.Points);
            }

            _leaderBoardNames.text = names;
            _leaderBoardScores.text = scores;
        }
    }
}
