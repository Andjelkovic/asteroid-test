﻿using Assets._GameAssets.Scripts.Core;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets._GameAssets.Scripts.Interface
{
    public class Hud : MonoBehaviour
    {
        [SerializeField]
        private Button _pauseButon;

        [SerializeField]
        private Text _score;

        [SerializeField]
        private Text _highScore;

        private SignalBus _signalBus;
        private GameData _gameData;

        [Inject]
        public void Construct(SignalBus signalBus, GameData gameData)
        {
            _gameData = gameData;
            _signalBus = signalBus;
            _signalBus.Subscribe<UpdateScoreSignal>(OnUpdateScore);
            _signalBus.Subscribe<StartGameSignal>(OnGameStarted);
        }

        private void OnGameStarted()
        {
            _highScore.text = _gameData.HighScore.ToString();
        }

        private void OnEnable()
        {
            _pauseButon.onClick.AddListener(OnPauseClicked);
        }

        private void OnDisable()
        {
            _pauseButon.onClick.RemoveAllListeners();
        }

        private void OnPauseClicked()
        {
            _gameData.IsPaused = !_gameData.IsPaused;
            _signalBus.Fire(new PauseSignal());
        }

        private void OnUpdateScore(UpdateScoreSignal scoreObject)
        {
            _score.text = _gameData.CurrentScore.ToString();
        }
    }
}
