﻿using Assets._GameAssets.Scripts.Core;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Assets._GameAssets.Scripts.Interface
{
    public class GameOverMenu : MonoBehaviour
    {
        [SerializeField]
        private Button _backToMenuButon;

        [SerializeField]
        private Button _retryButon;

        [SerializeField]
        private Text _finalScoreText;

        private SignalBus _signalBus;
        private GameData _gameData;
        private CanvasGroup _canvasGroup;

        [Inject]
        public void Construct(SignalBus signalBus, GameData gameData)
        {
            _gameData = gameData;
            _signalBus = signalBus;
            _signalBus.Subscribe<EndGameSignal>(OnEndGame);
        }

        private void OnEnable()
        {
            _backToMenuButon.onClick.AddListener(OnBackToMenuClicked);
            _retryButon.onClick.AddListener(OnRetryClicked);
        }

        private void OnDisable()
        {
            _backToMenuButon.onClick.RemoveAllListeners();
            _retryButon.onClick.RemoveAllListeners();
        }

        private void Start()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            _canvasGroup.alpha = 0;
            _canvasGroup.blocksRaycasts = false;
        }

        private void OnEndGame()
        {
            _canvasGroup.alpha = 1;
            _canvasGroup.blocksRaycasts = true;
            _finalScoreText.text = _gameData.CurrentScore.ToString();
        }

        private void OnBackToMenuClicked()
        {
            SceneManager.LoadScene(Strings.MainMenuScene);
        }

        private void OnRetryClicked()
        {
            SceneManager.LoadScene(Strings.GameplayScene);
        }

    }
}
