﻿using Assets._GameAssets.Scripts.Core;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Assets._GameAssets.Scripts.Interface
{
    public class PauseMenu : MonoBehaviour
    {
        [SerializeField]
        private Button _backToMenuButon;

        [SerializeField]
        private Button _quitButon;

        private SignalBus _signalBus;
        private GameData _gameData;
        private CanvasGroup _canvasGroup;

        [Inject]
        public void Construct(SignalBus signalBus, GameData gameData)
        {
            _gameData = gameData;
            _signalBus = signalBus;
            _signalBus.Subscribe<PauseSignal>(OnPause);
        }

        private void OnEnable()
        {
            _backToMenuButon.onClick.AddListener(OnBackToMenuClicked);
            _quitButon.onClick.AddListener(OnQuitClicked);
        }

        private void OnDisable()
        {
            _backToMenuButon.onClick.RemoveAllListeners();
            _quitButon.onClick.RemoveAllListeners();
        }

        private void Start()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            OnPause();
        }

        private void OnPause()
        {
            if (_gameData.IsPaused)
            {
                _canvasGroup.alpha = 1;
                _canvasGroup.blocksRaycasts = true;
            }
            else
            {
                _canvasGroup.alpha = 0;
                _canvasGroup.blocksRaycasts = false;
            }
        }

        private void OnBackToMenuClicked()
        {
            SceneManager.LoadScene(Strings.MainMenuScene);
        }

        private void OnQuitClicked()
        {
            // this wont work in editor
            Application.Quit();
        }
    }
}
