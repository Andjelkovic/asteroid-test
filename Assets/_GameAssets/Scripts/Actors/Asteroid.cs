﻿using Assets._GameAssets.Scripts.Core;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using Zenject;

namespace Assets._GameAssets.Scripts.Actors
{
    public class Asteroid : MonoBehaviour
    {
        private Vector3 _direction;

        private GameData _gameData;
        private SignalBus _signalBus;
        private bool _enabled;

        [Inject]
        public void Construct(GameData gameData, SignalBus signalBus)
        {
            _gameData = gameData;
            _signalBus = signalBus;
            _enabled = true;
            _signalBus.Subscribe<PauseSignal>(OnPause);
        }

        private void Start()
        {
            _direction = transform.up * Random.Range(3.0f * Time.deltaTime, 8.0f * Time.deltaTime);
            _enabled = !_gameData.IsPaused;
            Destroy(gameObject, 5f);
        }

        private void Update()
        {
            if (!_enabled)
                return;

            transform.position += _direction;
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag.Equals("Bullet"))
            {
                _gameData.CurrentScore += GlobalSettings.KillPoints;
                _signalBus.Fire(new UpdateScoreSignal {Points = _gameData.CurrentScore });
                Destroy(other.gameObject);
                Destroy(gameObject);
            }

            if (other.gameObject.tag.Equals("SpaceShip"))
            {
                _enabled = false;
                _signalBus.Fire(new EndGameSignal());
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }

        private void OnPause()
        {
            _enabled = !_gameData.IsPaused;
        }
    }
}
