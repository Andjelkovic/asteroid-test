﻿using Assets._GameAssets.Scripts.Core;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using Zenject;

namespace Assets._GameAssets.Scripts.Actors
{
    public class Bullet : MonoBehaviour
    {
        private GameData _gameData;
        private Vector3 _direction;
        private float _lifeTime = 2f;
        private float _speed = 15f;
        private SignalBus _signalBus;
        private bool _enabled;

        [Inject]
        public void Construct(GameData gameData, SignalBus signalBus)
        {
            _gameData = gameData;
            _signalBus = signalBus;
            _signalBus.Subscribe<EndGameSignal>(OnEndGame);
        }

        public void Initialize(Vector3 direction)
        {
            _direction = direction;
            _enabled = true;
        }
        
        void Update ()
        {
            if (_gameData.IsPaused || !_enabled)
                return;

            transform.position += _direction.normalized * _speed * Time.deltaTime;

            _lifeTime -= Time.deltaTime;
            if(_lifeTime < 0)
                Destroy(gameObject);
        }

        private void OnEndGame()
        {
            _enabled = false;
        }
    }
}
