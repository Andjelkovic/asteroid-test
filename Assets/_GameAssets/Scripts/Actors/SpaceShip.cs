﻿using System;
using Assets._GameAssets.Scripts.Core;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using Zenject;

namespace Assets._GameAssets.Scripts.Actors
{
    public class SpaceShip : MonoBehaviour
    {
        [SerializeField]
        private GameObject _bullet;

        private float _speed = 3f;
        private Vector3 _direction;

        private float _throttle;
        private LineRenderer _laser;

        private GameData _gameData;
        private SignalBus _signalBus;
        private bool _enabled;

        [Inject]
        public void Construct(GameData gameData, SignalBus signalBus)
        {
            _gameData = gameData;
            _signalBus = signalBus;
            _signalBus.Subscribe<StartGameSignal>(OnGameStarted);
            _signalBus.Subscribe<EndGameSignal>(OnEndGame);
        }

        void Start()
        {
            _laser = GetComponent<LineRenderer>();
            _laser.material = new Material(Shader.Find("Sprites/Default"));
            _laser.enabled = true;
            _laser.positionCount = 2;
        }

        void Update()
        {
            if (_gameData.IsPaused || !_enabled)
                return;

            UpdateMovement();
            UpdateDirection();
            UpdateShooting();
        }

        private void UpdateMovement()
        {
            _throttle = Mathf.Lerp(_throttle, Input.GetAxisRaw("Vertical"), 3f * Time.deltaTime);
            transform.position += new Vector3(_direction.x, _direction.y, 0).normalized * _throttle * _speed * Time.deltaTime;
        }

        private void UpdateDirection()
        {
            var worldpos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
            _direction = Vector3.Lerp(_direction, worldpos - transform.position, 3f * Time.deltaTime);
            var rotationZ = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg - 90f;
            transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);

            _laser.SetPosition(0, transform.position);
            _laser.SetPosition(1, new Vector3(_direction.x, _direction.y, 0).normalized * 10 + transform.position);
        }

        private void UpdateShooting()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var instantiatedBullet = Instantiate(_bullet, new Vector3(transform.position.x, transform.position.y, 0), transform.rotation);
                instantiatedBullet.GetComponent<Bullet>().Initialize(new Vector3(_direction.x, _direction.y, 0));
            }
        }

        private void OnGameStarted()
        {
            _enabled = true;
        }

        private void OnEndGame()
        {
            _enabled = false;
        }
    }
}
