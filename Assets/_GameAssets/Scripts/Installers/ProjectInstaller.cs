﻿using System;
using Assets._GameAssets.Scripts.Core;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using Zenject;

namespace Assets._GameAssets.Scripts.Installers
{
    public class ProjectInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            Container.DeclareSignal<StartGameSignal>();
            Container.DeclareSignal<EndGameSignal>();
            Container.DeclareSignal<PauseSignal>();
            Container.DeclareSignal<UpdateScoreSignal>();

            Container.Bind<GameData>().AsSingle().NonLazy();
            Container.Bind<ScoreHandler>().AsSingle().NonLazy();
            Container.BindInterfacesTo<ApplicationStartHandler>().AsSingle().NonLazy();
        }
    }
}