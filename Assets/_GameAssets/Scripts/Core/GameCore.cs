﻿using System.Collections;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets._GameAssets.Scripts.Core
{
    public class GameCore : MonoBehaviour
    {
        [SerializeField]
        private GameObject _asteroid;

        [SerializeField]
        private GameObject _infoText;

        private GameData _gameData;
        private SignalBus _signalBus;

        private int _numberOfAsteroids = 5;
        private int _asteroidsIncrement = 3;
        private int _asteroidsSpawnInterval = 3;

        // this can be done in many ways, I made 2 arrays which represent spawn positions of the asteroid
        private float[] _xRanges = { -14, -12, -9, 9, 12, 14 };
        private float[] _yRanges = { -5, -6, -7, 5, 6, 7 };

        [Inject]
        public void Construct(SignalBus signalBus, GameData gameData)
        {
            _gameData = gameData;
            _signalBus = signalBus;
            _signalBus.Subscribe<EndGameSignal>(OnEndGame);
        }

        private void Start()
        {
            StartCoroutine(DelayedGameStart());
            InvokeRepeating("PrepareNewAsteroids", 3, _asteroidsSpawnInterval);
        }

        private IEnumerator DelayedGameStart()
        {
            yield return new WaitForSeconds(2f);
            _infoText.SetActive(false);
            _signalBus.Fire(new StartGameSignal());
        }

        private void PrepareNewAsteroids()
        {
            for (var i = 0; i < _numberOfAsteroids; i++)
            {
                Instantiate(_asteroid, new Vector3(_xRanges[Random.Range(0, _xRanges.Length)], _yRanges[Random.Range(0, _yRanges.Length)], 0), Quaternion.Euler(0, 0, Random.Range(-0.0f, 359.0f)));
            }
            _numberOfAsteroids += _asteroidsIncrement;
        }

        private void OnEndGame()
        {
            _infoText.SetActive(true);
            _infoText.GetComponent<Text>().text = Strings.GameOver;
            CancelInvoke();
        }
    }
}
