﻿using System.Linq;
using Assets._GameAssets.Scripts.Data;
using UnityEngine;
using Zenject;

namespace Assets._GameAssets.Scripts.Core
{
    public class ScoreHandler
    {
        private Score _currentScoreData;
        private GameData _gameData;
        private SignalBus _signalBus;

        public ScoreHandler(GameData gameData, SignalBus signalBus)
        {
            _gameData = gameData;
            _signalBus = signalBus;
            _signalBus.Subscribe<UpdateScoreSignal>(OnUpdateScore);
            _signalBus.Subscribe<StartGameSignal>(OnStartGame);
            _signalBus.Subscribe<EndGameSignal>(OnEndGame);
        }

        private void OnStartGame()
        {
            _gameData.CurrentScore = 0;
            _currentScoreData = new Score(_gameData.PlayerName);
        }

        private void OnUpdateScore(UpdateScoreSignal scoreObject)
        {
            _currentScoreData.Points = scoreObject.Points;
        }

        private void OnEndGame()
        {
            _gameData.Leaderboard.Add(new Score
            {
                PlayerName = _currentScoreData.PlayerName,
                Id = _currentScoreData.Id,
                Points = _currentScoreData.Points
            });

            _gameData.HighScore = _gameData.Leaderboard.Max(item => item.Points);
        }
    }
}
