﻿namespace Assets._GameAssets.Scripts.Core
{
    public static class Strings
    {
        public static string MainMenuScene = "Menu";
        public static string GameplayScene = "Gameplay";

        public static string GetReady = "Get Ready!";
        public static string GameOver = "Game Over!";
    }
}
