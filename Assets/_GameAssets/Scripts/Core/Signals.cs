﻿namespace Assets._GameAssets.Scripts.Core
{
    public class StartGameSignal { }
    public class EndGameSignal { }
    public class PauseSignal
    {
        public bool IsPaused;
    }

    public class UpdateScoreSignal
    {
        public int Points;
    }

}
