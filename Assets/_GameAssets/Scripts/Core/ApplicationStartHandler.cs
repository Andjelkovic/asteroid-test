﻿using UnityEngine;
using Zenject;

namespace Assets._GameAssets.Scripts.Core
{
    /// <summary>
    /// Use this class for things which need to happen on app start
    /// </summary>
    public class ApplicationStartHandler : IInitializable
    {
        public void Initialize()
        {
            
        }
    }
}
